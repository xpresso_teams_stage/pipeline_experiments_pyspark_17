## Default Makefile generated for the  project : dnn_library
##


branch_name := `git branch | grep \* | cut -d ' ' -f2`
branch_name_str := `git branch | grep \* | cut -d ' ' -f2 | tr -d '/\ '`

build_folder := ${CURDIR}/xprbuild
docker_build_folder := ${build_folder}/docker
k8_build_folder := ${build_folder}/docker
linux_build_folder := ${build_folder}/system/linux
windows_build_folder := ${build_folder}/system/windows

PROJECT_VERSION := $(shell cat VERSION)
DOCKER_REGISTRY=dockerregistry.xpresso.ai

export PROJECT_VERSION
export ROOT_FOLDER := ${CURDIR}
export PROJECT_NAME := dnn_library

default: debug

# Clean the projects
clean:
	@echo "--------------- Clean Started ----------------"
	@echo "--------------- Clean Completed ----------------"

clobber: clean
	@echo "--------------- Clobber Started ----------------"
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '*.pyo' -exec rm --force {} +
	find . -name '*~' -exec rm --force {} +
	@echo "--------------- Clobber Completed ----------------"

# Much needed git commands
checkout:
	git pull

update: clean checkout

push: clean
	git squash
	git push origin ${branch_name}

patch: clean
	@if [ -z "${ARGS}" ];then\
		echo "\nError: Pass remote branch for creating patch.\n--usage: make patch master";\
	else\
		echo  "Creating patch for following commits";\
		git log --pretty=oneline -1;\
		git format-patch ${ARGS} --stdout > ${branch_name_str}.patch;\
	fi;

# Perform test
lint:
	@echo "--------------- Quality Check Started ----------------"
	@echo "Sonar Project Tester"
	@echo "--------------- Quality Check Completed ----------------"

unittest:
	@echo "Performing Unit Test"
	/bin/bash ${linux_build_folder}/test.sh ${DOCKER_IMAGE_NAME} ${TAG}

apitest:
	@echo "Performing API Testing"
	/bin/bash  ${linux_build_folder}/test.sh

systemtest:
	@echo "Performing System Testing"

test-all: unittest apitest systemtest

# Build
prepare:
	@echo "Installing Dependencies"
	/bin/bash ${linux_build_folder}/pre-build.sh

install-local: clean
	pip uninstall dnn_library | true
	pip install -e .

install: install-local

all: debug release

build: module

dist: clobber
	@echo "Generate distribution"

# Deployment
dockerpush:
	@echo "Tagging the docker image and pushing"
	docker login ${DOCKER_REGISTRY} -u admin -p Abz00ba@123
	docker push ${DOCKER_IMAGE_NAME}:${TAG}

# utils
doc:
	@echo "This should generate the docs"

version:
	@echo "This should print the version"
	echo "${PROJECT_VERSION}"

.PHONY:
	clean all debug debug-local release deploy deploy-local run run-local
